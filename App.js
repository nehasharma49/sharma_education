import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
import Home from './src/screens/Home';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Contact from './src/screens/Contact';
import Course from './src/screens/Course';
import UserData from './src/screens/UserData';
import About from './src/screens/About';

import {
  useFonts,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
} from "@expo-google-fonts/josefin-sans";
import { Nunito_600SemiBold, Nunito_700Bold } from "@expo-google-fonts/nunito";
import AppLoading from "expo-app-loading";




export default function App() {
  const Stack = createNativeStackNavigator();

  let [fontsLoaded] = useFonts({
    JosefinSans_400Regular,
  JosefinSans_500Medium,
  Nunito_600SemiBold, Nunito_700Bold 
  });

  if(!fontsLoaded){
    <AppLoading/>
  }

  return (

    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" options={{ headerShown: false }} component={Home} />

        {/*Course Screen*/}
        <Stack.Screen name="Course"
          options={{
            headerTitleStyle: {
              fontSize: 25,
              fontFamily: "Nunito_600SemiBold",
            },
            headerTitle: "Courses",
            headerTitleAlign: "center"
          }} component={Course} />

        {/*UserData Screen*/}
        <Stack.Screen name="Student"
          options={{
            headerTitleStyle: {
              fontSize: 25,
              fontFamily: "Nunito_600SemiBold",
            },
            headerTitle: "Student",
            headerTitleAlign: "center"
          }}
          component={UserData} />

        {/*About Screen*/}
        <Stack.Screen name="About"
        options={{
          headerTitleStyle: {
            fontSize: 25,
            fontFamily: "Nunito_600SemiBold",
          },
          headerTitle: "About",
          headerTitleAlign: "center"
        }}
        component={About} />

        {/*Contact Screen*/}
        <Stack.Screen name="Contact" 
        options={{
          headerTitleStyle: {
            fontSize: 25,
            fontFamily: "Nunito_600SemiBold",
          },
          headerTitle: "Contact Us",
          headerTitleAlign: "center"
        }}
        component={Contact} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

