import { View, Text, FlatList, StyleSheet, Image } from "react-native";
import React, { useState, useEffect } from "react";

const UserData = () => {
 
  const [myData, setMyData] = useState([]);

  const getUserData = async () => {
    try {
      const response = await fetch(
        "https://thapatechnical.github.io/userapi/users.json"
      );
      const realData = await response.json();
      setMyData(realData);
      
      // console.log(realData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => getUserData(), []);

   
  
    
  return (
    <View>
      <Text>List of Students</Text>
      <FlatList
      data={myData}
      renderItem={() =>{}}
      
      />
    </View>
  )
}

export default UserData

const styles = StyleSheet.create({})